// Configuration du provider
provider "google" {
 credentials = "${file("Dev-Opsv3-account.json")}"
 project     = "dev-opsv3"
 region      = "europe-west1"
 zone        = "europe-west1-b"
}

//Création du réseau
  
resource "google_compute_network" "default" {
  name                    = "my-network"
  auto_create_subnetworks = "true"
}

//Création des règles firewall

resource "google_compute_firewall" "default" {
  name    = "my-firewall"
  network = "${google_compute_network.default.name}"
  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "443", "8080"]
  }

}

//Création des instances + clé SSH
resource "google_compute_instance" "vm_instance" {
  count        = 3
  name         = "projet-${count.index}"
  machine_type = "n1-standard-2"
  zone         = "europe-west1-b"

//metadata {
//sshKeys = "luvsgrim:${file("google ssh.pub")}"
//sshKeys = "ubuntu:${file("devops.pub")}"
//}

  boot_disk {
    initialize_params {
      image       = "ubuntu-1604-xenial-v20181114"
      size        = 30
    }
  }
  
  network_interface {
	network       = "${google_compute_network.default.name}"
    access_config = {
    }
  }
}